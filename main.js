export function setup(ctx) {
    const id = 'semi-auto-mine';

    const orePriority = game.mining.actions.allObjects.reverse()

    const adjustSelectedRock = (selectedRock = undefined) => {
        // Are we even mining right now?
        if(game.activeAction !== game.mining) {
            return;
        }

        // If yes, start looking for an open rock
        orePriority.every(ore => {
            if(game.mining.canMineOre(ore) && !ore.isRespawning && ore.currentHP > 0) {
                // Prevents an edge case where the onRockClick we are catching is the one thrown by this function. Exits either way.
                // Also prevents an error where the game attempts to select our currently selected rock, stopping mining
                if(ore !== selectedRock && game.mining.selectedRock !== ore) {
                    game.mining.onRockClick(ore)
                }
                return false;
            }
            return true;
        })
    }

    // After a rock respawns, check if we want to switch back to it
    ctx.patch(Mining, 'respawnRock').after(() => {
        adjustSelectedRock()
    })

    // We also want to check if our rock runs out of hp
    ctx.patch(Mining, 'startRespawningRock').after(() => {
        adjustSelectedRock()
    })

    // If we select a depleted rock, it should also move over
    ctx.patch(Mining, 'onRockClick').before((rock) => {
        adjustSelectedRock(rock)
    }) 
    mod.api.SEMI.log(id, 'Successfully loaded!')
}
